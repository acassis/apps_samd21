Introduction
============

This is a circular log buffer infrastructure to use as a store and transmit
data. Normally before transmitting data to a server you want to store it
locally to avoid losing data, case the server is not available.


Rational
========

This is a circular log buffer where you can define if it will stop saving data
after it becoming full or if will overwrite the oldest data in the log file.

We can have many possibilities to log file status, consider a log file that can
support up to 10 packest.

Packets positions in the logfile:
---------------------------------

[0123456789]

Initial:
--------

[] - Log file empty (just created the file)

Writing data without reading yet:
---------------------------------

[NNNN]

Note: N = new packets saved on log file

When start transmitting packets before log file overflow:
---------------------------------------------------------

[TTNNNNN]

Note: T = transmitted packet, it is marked as removed in the log file.

If transmittion didn't start we could get full log file:
--------------------------------------------------------

[NNNNNNNNNN]

Note: all 10 positions are full and the log file couldn't accept new packets.

If after filling all position we start to transmit but it didn't start inserting new packets:
---------------------------------------------------------------------------------------------

[TTTTTNNNNN]

If it didn't start inserting new data, it could become empty (full of transmitted data):
----------------------------------------------------------------------------------------

[TTTTTTTTTT]

And then it could start inserting new data again:
-------------------------------------------------

[NNNNTTTTTT]

Again the device could start to transmit and we could have this configuration:
------------------------------------------------------------------------------

[TTTNNNNTTT]

Also we could have the last scenario:
-------------------------------------

[NNTTTTNNNN]

So, as you can see we could have a log file with many configurations and it is a
little bit hard to figure out where to start inserting new packets and from what
position should start transmitting the oldest saved packet.

This log buffer try to simplify it for the developers. All you need to do is:

/* Initialize the log system */

init_log("/mnt/mypackets.db");

/* Read the last saved packet counter */

pktcnt = getlastpkt();
save_pkt.pktcounter = pktcnt;

/* Insert the packet */

insert_entry(&save_pkt);

/* Read the oldest saved not transmitted packet */

get_entry(&load_pkt);

/* Transmit the loaded packet using you medium interface (ethernet, wifi, lora, sigfox, etc */

/* After confirmation that the packet was trasmitted, remove it! */

remove_entry(&load_pkt);


