/****************************************************************************
 * apps/fsutils/logbuf/logbuf.c
 *
 *   Copyright (C) 2019 Alan Carvalho de Assis. All rights reserved.
 *   Author: Alan Carvalho de Assis <acassis@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <crc8.h>

#include "fsutils/logbuf.h"

#define PACKETSIZE     24            /* Size of the packet to save (logentry)  */
#define MAXPKTNUM      10922         /* Max number of packets to store         */
#define DBFILE         "/dev/at24"   /* Path and file name of the log database, in this case the EEPROM */

/* This is the control structure for the log buffer */

struct logbuf
{
  int pkt_cnt;     /* Packet counter */
  int data_in;     /* Position to write next data */
  int data_out;    /* Position to read next data */
  int full;        /* Buffer full status */
  int fdlog;       /* log file descriptor */
  logentry pkt;    /* user packet structure */
};

struct logbuf g_logbuf;

/* Comment this line to disable debug */

#define DEBUG_LOG 1

/* These lines shouldn't be commented */

#ifdef DEBUG_LOG
#  define dbg printf
#else
#  define dbg
#endif

/* This flag controls if we will print all packets status */

#define DEBUG_LOG_ALLPKTS 1

/* This function will read a packet at position #pos */

int read_position(int pos)
{
  int ret;

  dbg("Going the read a packet from position %d\n", pos);

  /* Move to packet position 'pos', multiply by sizeof(logentry) to find it */

  ret = lseek(g_logbuf.fdlog, (pos * sizeof(logentry)), 0);
  if (ret < 0)
    {
      dbg("Error: moving to file position %d!\n", pos);
      return ret;
    }

  /* Read a packet and check if we read enough bytes */

  ret = read(g_logbuf.fdlog, &g_logbuf.pkt, sizeof(logentry));
  if (ret != sizeof(logentry))
    {
      dbg("Error: fread returned wrong amount of byte. Read %d and logentry size = %ld!\n", ret, sizeof(logentry));
      ret = -1;
    }

  return ret;
}

int log_init (void)
{
  int ret;
  int filesz = 0; /* Size of log file, also will store number packets */
  int first  = 0; /* Left position of the binary search logic */
  int last   = 0; /* Right position of the binary search logic */
  int mid    = 0; /* Middle position in the binary search logic */
  int sorted = 1; /* Initially assume the packets are sorted */
  int i;
  int val_first;  /* Value of the packet counter at the Left position */
  int val_mid;    /* Value of the packet counter at the Middle position */
  int val_last;   /* Value of the packet counter at the Right position */
  int val_bigger; /* Value of the biggest packet counter during the search */
  int pos_bigger; /* Position of the biggest packet counter in the file */
  int flag_first;

  /* Oldest removed and latest inserted packets */

  uint32_t smaller = 0xffffffff;
  uint32_t counter;

  /* Initialize the global logbuf structure data */

  g_logbuf.data_in  = -1;
  g_logbuf.data_out = -1;
  g_logbuf.full     = 0;

  /* Sane check, verify if pkt size is PACKETSIZE bytes */

  if (sizeof(logentry) != PACKETSIZE)
    {
      dbg("Error: packet size = %d! Should be = %d!\n",
          (int) sizeof(logentry), PACKETSIZE);
      return -1;
    }

  /* Try to open file to read and write */

  g_logbuf.fdlog = open(DBFILE, O_RDWR);
  if (g_logbuf.fdlog < 0)
    {
      /* Try to create it */

      g_logbuf.fdlog = open(DBFILE, O_RDWR|O_CREAT|O_TRUNC);
      if (g_logbuf.fdlog < 0)
      {
        dbg("Error %d creating log file!", errno);
        return -1;
      }
    }

  /* Get the file size */

  filesz = lseek(g_logbuf.fdlog, 0L, SEEK_END);
  if (filesz < 0)
    {
      dbg("Cannot get size of file, error = %d!\n", errno);
      ret = -1;
      goto leave_close_file;
    }

  /* If file is empty then nothing to do */

  if (filesz == 0)
    {
      dbg("File is empty, we just create it!\n");
      g_logbuf.data_in = 0;
      g_logbuf.data_out = 0;
      goto leave_close_file;
    }

  /* Check if file size is bigger than MAXPKTNUM */

  if (filesz > (MAXPKTNUM * sizeof(logentry)))
    {
      dbg("Warning: File size is bigger than Max log size\n");
      dbg("File size = %ld\n", filesz);
      //ret = -1;
      //goto leave_close_file;
    }

  /* Move the file descriptor to beginning of file */

  lseek(g_logbuf.fdlog, 0L, SEEK_SET);

  /* Read the first packet */

  ret = read_position(0);
  if (ret < 0)
    {
      dbg("Error: failed to read first packet at position 0!\n");
      goto leave_close_file;
    }

  counter    = g_logbuf.pkt.pktcounter;
  val_bigger = counter;
  pos_bigger = 0;

  /* filesz now will store the quantity of packets in the file */

  filesz = filesz / sizeof(logentry);

  dbg("There are %d packet(s) in the database!\n", filesz);

  /* Print all positions for debug purpose */

#ifdef DEBUG_LOG_ALLPKTS
  for (i = 0; i < filesz; i++)
    {
      ret = read_position(i);

      dbg("Pos %08d = 0x%02X | PKTCNT = %08d\n", i, g_logbuf.pkt.sent_flag,
                                                 g_logbuf.pkt.pktcounter);
    }
#endif

  /* Assume the last packet in the file is the last saved packet.
   * Because this is a circular log file, the last saved packet could be at any
   * position inside the log file. We will discover the truth after the binary
   * search. */

  last = filesz - 1;

  /* Search for the bigest packet counter in the log file */

  while (first <= last)
    {

      ret = read_position(first);
      if (ret < 0)
        {
          dbg("Error: failed to read first packet at position %d!\n", first);
          goto leave_close_file;
        }

      val_first = g_logbuf.pkt.pktcounter;

      ret = read_position(last);
      if (ret < 0)
        {
          dbg("Error: failed to read last packet at position %d!\n", last);
          goto leave_close_file;
        }

      val_last = g_logbuf.pkt.pktcounter;

      mid = (first + last) / 2;

      ret = read_position(mid);
      if (ret < 0)
        {
          dbg("Error: failed to read middle packet at position %d!\n", mid);
          goto leave_close_file;
        }

      /* Read the middle packet number */

      val_mid = g_logbuf.pkt.pktcounter;

      /* Verify if packets are ordered, it means that log file didn't overflow yet */

      if (val_first < val_mid && val_mid < val_last && sorted)
        {
          pos_bigger = last;
          val_bigger = val_last;
          break;
        }

      sorted = 0;

      /* Do we found the last saved packet? */

      if ((last-first) == 1)
        {
          /* Yes, but which one is the bigger value (last packet inserted) ? */

          if (val_first > val_last)
            {
              pos_bigger = first;
              val_bigger = val_first;
              break;
            }
          else
            {
              pos_bigger = last;
              val_bigger = val_last;
              break;
            }
        }

      /* Is the first half ("left side") ordered? */

      if (val_first > val_mid)
        {
          /* Yes, then the new "last" will receive the "middle" position */

          last = mid;
        }
      else
        {
          /* No, then the new "first" will receive the "middle" position */

          first = mid;
        }

    } /* end of while */

  /* Save the bigger packet counter (counter value of the last packet inserted in the database) */

  g_logbuf.pkt_cnt = val_bigger;

  dbg("Biggest packet #%d found at position %d\n", val_bigger, pos_bigger);

  /* If flag at pos_bigger is TXDPACKET (sent packet) then there isn't pkt to send */

  ret = read_position(pos_bigger);
  if (ret < 0)
    {
      dbg("Error: failed to read pos_bigger packet at position %d!\n", pos_bigger);
      goto leave_close_file;
    }

  if (g_logbuf.pkt.sent_flag == TXDPACKET)
    {
      /* We could start saving at position 0, because there is no packet to transmit.
       * But suppose the storage device is a flash memory, we could damage the begining
       * always restarting at position 0 of the flash memory, so let to start after the
       * position of the past saved and transmitted packet */

      g_logbuf.data_in  = ((pos_bigger + 1) % MAXPKTNUM);
      g_logbuf.data_out = ((pos_bigger + 1) % filesz);
      goto leave_close_file;
    }

  /* If filesz is smaller than MAXPKTNUM then data_in will be at pos_bigger + 1 */

  if (filesz < MAXPKTNUM)
    {
      g_logbuf.data_in = pos_bigger + 1;
      goto search_old_data;
    }

  /* The oldest transmitted packet is after to bigger packet.
   * We will store new packets at this position */

  g_logbuf.data_in = ((pos_bigger + 1) % MAXPKTNUM);

  /* If at this new data insert position ("data_in") we have a NEWPACKET (not sent packet), then it is full */

  ret = read_position(g_logbuf.data_in);
  if (ret < 0)
    {
      dbg("Error: failed to read data_in packet at position %d!\n", g_logbuf.data_in);
      goto leave_close_file;
    }

  if (g_logbuf.pkt.sent_flag == NEWPACKET)
    {
      /* TODO: This is a double check, could be eliminated? */

      if ((filesz >= (MAXPKTNUM - 1)))
        {
          g_logbuf.full = 1;
        }

      /* If it is at last position then data_out starts at 0 */

      if (g_logbuf.data_in == (MAXPKTNUM - 1))
        {
          g_logbuf.data_out = 0;
        }
      else
        {
          g_logbuf.data_out = g_logbuf.data_in;
        }

      goto leave_close_file;
    }

  /* Now search for the oldest not transmitted packet */

search_old_data:

  first = 0;
  last = filesz - 1;

  /* If packet at last position is NEWPACKET then oldest is before that */

  ret = read_position(last);
  if (ret < 0)
    {
      dbg("Error: failed to read last packet at position %d!\n", last);
      goto leave_close_file;
    }

  if (g_logbuf.pkt.sent_flag == NEWPACKET)
    {
      /* If data_in is not at last position, then we can start from data_in */

      if (g_logbuf.data_in < last)
        {
          first = g_logbuf.data_in;
        }
    }
  else
    {
      if (pos_bigger == 0)
        {
          g_logbuf.data_in = 1;
          g_logbuf.data_out = 0;
          goto leave_close_file;
        }
      else
        {
          last = pos_bigger;
        }
    }

  smaller = val_bigger;

  /* Search for the packet with small ID (pktcounter) not sent yet */

  while (first <= last)
    {
      ret = read_position(first);
      if (ret < 0)
        {
          dbg("Error: failed to read first packet at position %d!\n", first);
          goto leave_close_file;
        }

      val_first  = g_logbuf.pkt.pktcounter;
      flag_first = g_logbuf.pkt.sent_flag;

      ret = read_position(last);
      if (ret < 0)
        {
          dbg("Error: failed to read last packet at position %d!\n", last);
          goto leave_close_file;
        }

      val_last = g_logbuf.pkt.pktcounter;

      mid = (first + last) / 2;

      ret = read_position(mid);
      if (ret < 0)
        {
          dbg("Error: failed to read middle packet at position %d!\n", mid);
          goto leave_close_file;
        }

      val_mid = g_logbuf.pkt.pktcounter;

      /* Do we find it? */

      if ((last-first) == 1)
        {
          if (flag_first == NEWPACKET)
            {
              g_logbuf.data_out = first;
            }
          else
            {
              g_logbuf.data_out = last;
            }
          break;
        }

      /* Is it a packet we are looking for? */

      if (g_logbuf.pkt.sent_flag == NEWPACKET)
        {
              if (g_logbuf.pkt.pktcounter <= smaller)
                {
                  smaller = g_logbuf.pkt.pktcounter;
                  g_logbuf.data_out = mid;
                }
              last = mid;
        }
       else
          {
            if (flag_first == TXDPACKET)
              {
                first = mid;
              }
          }
    }

leave_close_file:

  dbg("data_in = %d | data_out = %d | pkt_cnt = %d | full = %d\n",
      g_logbuf.data_in, g_logbuf.data_out, g_logbuf.pkt_cnt, g_logbuf.full);

  ret = 0;

  close(g_logbuf.fdlog);
  return ret;
}

uint32_t getlastpkt(void)
{
  return g_logbuf.pkt_cnt;
}

int insert_entry(logentry *le)
{
  /* Is the Buffer Overun? */

  if (g_logbuf.full)
    {
      return -1;
    }

  /* Try to open file to read */

  g_logbuf.fdlog = open(DBFILE, O_RDWR);
  if (g_logbuf.fdlog < 0)
    {
      dbg("Error %d opening log file!", errno);
      return -1;
    }

  dbg("Saving a new packet position to flash!\n");

  le->pktcounter = getlastpkt() + 1;

  /* Mark package as not sent */

  le->sent_flag = NEWPACKET;

  dbg("Saving packet %d at position %d!\n", le->pktcounter, g_logbuf.data_in);

  /* Create the CRC8 to doable check that packet was corrupted */

  le->crc = crc8((FAR const uint8_t *) le, 16); /* 4 (pktcount) + 12 (data) */

  /* Write it */

  lseek (g_logbuf.fdlog, g_logbuf.data_in * sizeof(logentry), 0);
  write (g_logbuf.fdlog, le, sizeof(logentry));

  g_logbuf.data_in++;

  if (g_logbuf.data_in >= MAXPKTNUM)
    {
      g_logbuf.data_in = 0;
    }

  if (g_logbuf.data_in == g_logbuf.data_out)
    {
      g_logbuf.full = 1;
    }

  close(g_logbuf.fdlog);

  return g_logbuf.full;
}

int get_entry(logentry *le)
{
  uint8_t newcrc;

  /* Is the buffer empty? */

  if (g_logbuf.data_in == g_logbuf.data_out && !g_logbuf.full)
    {
      return -1;
    }

  /* Try to open file to read/write */

  g_logbuf.fdlog = open(DBFILE, O_RDWR);
  if (g_logbuf.fdlog < 0)
    {
      dbg("Error %d opening log file!", errno);
      return -1;
    }

  /* Read a packet to send */

  lseek (g_logbuf.fdlog, g_logbuf.data_out * sizeof(logentry), 0);
  read(g_logbuf.fdlog, le, sizeof(logentry));

  /* Confirm that it was NOT SENT */

  if (le->sent_flag != NEWPACKET)
    {
      dbg("Error: position %d is not marked to send!\n", g_logbuf.data_out);
      return -1;
    }

  /* Check if the CRC is correct */

  newcrc = crc8((FAR const uint8_t *) le, 16);

  if (le->crc != newcrc)
    {
      dbg("Error: Invalid CRC! Expected 0x%02x read 0x%02x", le->crc, newcrc);
      return -1;
    }

  /* Close log file */

  close(g_logbuf.fdlog);

  return 0;
}

int remove_entry(logentry *le)
{
  uint8_t newcrc;

  /* Is the buffer empty? */

  if (g_logbuf.data_in == g_logbuf.data_out && !g_logbuf.full)
    {
      return -1;
    }

  /* Try to open file to read */

  g_logbuf.fdlog = open(DBFILE, O_RDWR);
  if (g_logbuf.fdlog < 0)
    {
      dbg("Error %d opening log file!", errno);
      return -1;
    }

  dbg("Reading a packet from log file!\n");

  /* Read a packet to send */

  lseek(g_logbuf.fdlog, g_logbuf.data_out * sizeof(logentry), 0);
  read(g_logbuf.fdlog, le, sizeof(logentry));

  /* Confirm that it was NOT SENT */

  if (le->sent_flag != NEWPACKET)
    {
      dbg("Error: position %d is not marked to send!\n", g_logbuf.data_out);
      return -1;
    }

  /* Check if the CRC is correct */

  newcrc = crc8((FAR const uint8_t *) le, 16);

  if (le->crc != newcrc)
    {
      dbg("Error: Invalid CRC! Expected 0x%02x read 0x%02x", le->crc, newcrc);
      return -1;
    }

  /* Mark this packet as sent */

  le->sent_flag = TXDPACKET;
  lseek(g_logbuf.fdlog, g_logbuf.data_out * sizeof(logentry), 0);
  write(g_logbuf.fdlog, le, sizeof(logentry));

  /* Close log file */

  close(g_logbuf.fdlog);

  g_logbuf.data_out++;

  if (g_logbuf.data_out >= MAXPKTNUM)
    {
      g_logbuf.data_out = 0;
    }

  g_logbuf.full = 0;

  return 0;
}

