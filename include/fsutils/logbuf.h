/****************************************************************************
 * apps/include/fsutils/logbuf.h
 *
 *   Copyright (C) 2019 Alan Carvalho de Assis. All rights reserved.
 *   Author: Alan Carvalho de Assis <acassis@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __APPS_INCLUDE_FSUTILS_LOGBUF_H
#define __APPS_INCLUDE_FSUTILS_LOGBUF_H 1

#include <stdint.h>

/* Status of a packet: NEW or TRANSMITTED */

#define NEWPACKET 0xAA
#define TXDPACKET 0x55

/* This is the packet structure that will be saved at the database file */

struct user_pkt_s
{
  /* Obrigatory fields */

  uint32_t pktcounter;       /* Packet counter for checking. 4 bytes */

  /* User data to store and to transmit */

  /* The sigfox max packet size. + 12 bytes */

  uint8_t data[12];

  /* The CRC to confirm that is not corrupted. + 1 byte */

  uint8_t crc;

  /* sent_flag and padding are not used to compute the CRC! */

  /* Was this Packet sent? + 1 byte */

  uint8_t  sent_flag;

  /* This structure needs to be multiple of 8 bytes, use padding to get it */

  uint8_t padding[6];
};

typedef struct user_pkt_s logentry;

/* Initialize the log system */

int log_init (void);

/* Get the value of the last packet in the log file */

uint32_t getlastpkt(void);

/* Insert a new packet in the log file */

int insert_entry(logentry *le);

/* Read the oldest packet from the log file */

int get_entry(logentry *le);

/* Remove the oldest packet from the log file */

int remove_entry(logentry *le);

#endif /* __APPS_INCLUDE_FSUTILS_LOGBUF_H */
