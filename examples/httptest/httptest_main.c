/****************************************************************************
 * examples/httptest/httptest_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * httptest_main
 ****************************************************************************/

#if defined(BUILD_MODULE)
int main(int argc, FAR char *argv[])
#else
int httptest_main(int argc, char *argv[])
#endif
{
  printf("Httptest, World!!\n");

    /* first what are we going to send and where are we going to send it? */
    int portno    = 80;
    //char *host    = "httpbin.org";
    char *host    = "52.71.234.219"; /* IP of httpbin.org */
    //char *message = "GET /ip HTTP/1.0\r\n\r\n";
    //char *message = "GET /get?variable=1 HTTP/1.0\r\n\r\n";
    char *message = "POST /post HTTP/1.0\r\n"
                    "Content-Type: application/x-www-form-urlencoded\r\n"
                    "Content-Length: 20\r\n\r\n"
                    "name=Nome&value=Alan\r\n";

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;
    char response[512];

    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
      {
        printf("ERROR opening socket\n");
        return -1;
      }

    /* lookup the ip address */
    server = gethostbyname(host);
    if (server == NULL)
      {
        printf("ERROR, no such host\n");
        return -1;
      }

    /* fill in the structure */
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    /* connect the socket */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
      {
        printf("ERROR connecting\n");
        return -1;
      }

    /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd,message+sent,total-sent);
        if (bytes < 0)
          {
            printf("ERROR writing message to socket\n");
            return -1;
          }
        if (bytes == 0)
          {
            break;
          }
        sent+=bytes;
    } while (sent < total);

    /* receive the response */
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
          {
            printf("ERROR reading response from socket\n");
            return -1;
          }

        if (bytes == 0)
          {
            break;
          }
        received+=bytes;
    } while (received < total);

    if (received == total)
      {
        printf("storing complete response from socket\n");
      }

    /* close the socket */
    close(sockfd);

    /* process response */
    printf("Response:\n%s\n", response);

    return 0;
}
