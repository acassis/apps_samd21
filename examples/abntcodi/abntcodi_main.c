/****************************************************************************
 * examples/hello/abntcodi_main.c
 *
 *   Copyright (C) 2018 Gregory Nutt. All rights reserved.
 *   Author: Alan Carvalho de Assis <acassis@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdio.h>
#include <fcntl.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

#include "industry/abnt_codi.h"


/****************************************************************************
 * Global variables
 ****************************************************************************/

int fd_codi;
int fd_sigfox;
FAR struct abnt_codi_proto_s *proto;

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

void print_abnt_codi(FAR struct abnt_codi_proto_s *protoc)
{
  printf("Seconds missing to end of the active demand..................: %d\n",
         protoc->end_act_dem);
  printf("Current Bill Indicator.......................................: %d\n",
         protoc->bill_indicator);
  printf("Reactive Interval Indicator..................................: %d\n",
         protoc->react_interval);
  printf("Capacitive Reactive Pulses are used to calculate consumption.: %s\n",
         boolstr(protoc->react_cap_pulse));
  printf("Inductive Reactive Pulses are used to calculate consumption..: %s\n",
         boolstr(protoc->react_ind_pulse));
  printf("Segment type.................................................: %s\n",
         protoc->segment_type == SEGMENT_PEEK     ? "PEEK"        :
         protoc->segment_type == SEGMENT_OUT_PEEK ? "OUT OF PEEK" :
         protoc->segment_type == SEGMENT_RESERVED ? "RESERVED"    : "UNKNOWN");
  printf("Charges type.................................................: %s\n",
         protoc->charge_type == CHARGES_BLUE        ? "BLUE"       :
         protoc->charge_type == CHARGES_GREEN       ? "GREEN"      :
         protoc->charge_type == CHARGES_IRRIGATORS  ? "IRRIGATORS" : "OTHERS");
  printf("Number of Active pulses since the beginning of current demand: %d\n",
         protoc->pulses_act_dem);
  printf("Number of Reactive pulses since beginning of current demand..: %d\n",
         protoc->pulses_react_dem);
}


  /* SIGFOX */

uint8_t u8_rx_shield;
char au8_sf_message[32];
uint8_t au8_rx_shield[32];
const uint8_t AT[]          = "AT\n";
const uint8_t AT_ID[]       = "AT$I=10\n";
const uint8_t AT_PAC[]      = "AT$I=11\n";
const uint8_t AT_RC[]       = "AT$RC\n";
const char AT_SF[]          = "AT$SF=";
const char AT_PAYLOAD[12];
uint8_t data[8];  //mudei aqui


void at_sigfox(int shield)
{
  int cont;
  int retn;

  /* AT - SIGFOX */

  cont=0;
  write(shield, &AT, strlen((char *)AT) );
  printf("%s", &AT);

  do
    {
      retn = read(shield, &u8_rx_shield, 1);
      if (retn != 1)
        {
          continue;
        }

      au8_rx_shield[cont++] = u8_rx_shield;
    } while((cont > 31) || (u8_rx_shield != '\n'));

  au8_rx_shield[cont] = '\0';
  printf("%s\n", &au8_rx_shield);
}

void id_sigfox(int shield)
{
  int cont;
  int retn;

  /* ID */

  cont=0;
  write(shield, &AT_ID, strlen((char *)AT_ID));
  printf("%s", &AT_ID);

  do
    {
      retn = read(shield, &u8_rx_shield, 1);
      if (retn != 1)
        {
          continue;
        }

      au8_rx_shield[cont++] = u8_rx_shield;
    }while((cont > 31) || (u8_rx_shield != '\n'));

  au8_rx_shield[cont] = '\0';
  printf("%s", &au8_rx_shield);
}

void pac_sigfox(int shield)
{
  int cont;
  int retn;

  /* PAC */

  cont=0;
  write(shield, &AT_PAC, strlen((char *)AT_PAC) );
  printf("%s", &AT_PAC);

  do
    {
      retn = read(shield, &u8_rx_shield, 1);
      if (retn != 1)
        {
          continue;
        }

        au8_rx_shield[cont++] = u8_rx_shield;
    } while((cont > 31) || (u8_rx_shield != '\n'));

  au8_rx_shield[cont] = '\0';
  printf("%s", &au8_rx_shield);
}

void payload_sigfox(int shield)
{
  int cont;
  int retn;

  /* SIGFOX_SEND_PAYLOAD */

  cont=0;
  write(shield, &AT_RC, strlen((char *)AT_RC) );
  printf("%s", &AT_RC);

  do
    {
      retn = read(shield, &u8_rx_shield, 1);
      if (retn != 1)
        {
          continue;
        }

      au8_rx_shield[cont++] = u8_rx_shield;
    } while((cont > 31) || (u8_rx_shield != '\n'));

  au8_rx_shield[cont] = '\0';
  printf("%s", &au8_rx_shield);
}

void allocate_mem_sigfox(int shield)
{
  int cont;
  int retn;
  int len;

  /* Allocate memory to concat str */

  cont=0;
  au8_sf_message[0] = '\0';

  sprintf(au8_sf_message, "%s%ld%x%x%x%x%x%x%x%x\n", AT_SF, time(NULL),
             data[0], data[1], data[2], data[3],
             data[4], data[5], data[6], data[7]);

  len = strlen(au8_sf_message);

  printf("The PAYLOAD string size is %d \n", len);

  write(shield, &au8_sf_message, strlen(au8_sf_message) );
  printf("%s\n", &au8_sf_message);

  do
    {
      retn = read(shield, &u8_rx_shield, 1);
      if (retn != 1)
        {
          continue;
        }

        au8_rx_shield[cont++] = u8_rx_shield;
    }while((cont > 31) || (u8_rx_shield != '\n'));

  au8_rx_shield[cont] = '\0';
  printf("%s\n", &au8_rx_shield);
}

void communication_sigfox(int shield)
{
  at_sigfox(shield);
  id_sigfox(shield);
  pac_sigfox(shield);
  payload_sigfox(shield);
  allocate_mem_sigfox(shield);
}

int sigfox_tx_data(void *p)
{
  /* Run forever */

  for (;;)
    {
      /* Read until we complete a sequence of 8 bytes */

      communication_sigfox(fd_sigfox);

      usleep(300000000);
    }
}

int sigfox_init(const char *dev)
{
  int ret;
  pthread_t id_sigfox;

  /* Open the serial port used for sigfox */

  fd_sigfox = open(dev, O_RDWR);
  if (fd_sigfox < 0)
    {
      printf(/*LOG_ERR,*/ "Error openning %s!\n", dev);
      ret = errno;
      goto init_file_err;
    }

  ret = pthread_create(&id_sigfox, NULL, (pthread_startroutine_t)sigfox_tx_data, NULL);
  if (ret != 0)
    {
      printf(/*LOG_ERR,*/ "codi_init: Failed to create codi poll thread, error=%d\n", ret);
      ret = -1;
      goto init_thread_err;
    }

  return OK;

init_thread_err:
  close(fd_sigfox);
init_file_err:

  return ret;
}


int codi_poll_data(void *p)
{
  int i;
  int ret;
  int cnt = 0;
  int idx = 0;
  int act_dem_ant = 0;
  uint8_t byte;
  uint8_t tmp[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  bool issync = false;
  unsigned int err_count = 0;

  /* Run forever */

  for (;;)
    {
      /* This loop will copy only a byte at a time if not synchronized
       * or 8 bytes at a time if it is synchronized.
       */

      i = 0;

      do
        {
          ret = read(fd_codi, &byte, 1);
          if (ret != 1)
            {
              continue;
            }

          /* Save a byte and increase the circular buffer */

          tmp[idx] = byte;
          idx++;
          idx = idx % 8;
          cnt++;
          i++;
        }
      while (issync && i < 8);

      /* Copy the circular buffer to a linear buffer */

      for (i = 0; i < 8; i++)
        {
          //printf("data[%d] = tmp[%d] = 0x%02x\n", i, ((cnt + i) % 8), tmp[((cnt + i) % 8)]);
          data[i] = tmp[((cnt + i) % 8)];
        }

      printf("\n\n\nCODI RECEIVED: %02X %02X %02X %02X %02X %02X %02X %02X SYNC = %d\n",
             data[0], data[1], data[2], data[3],
             data[4], data[5], data[6], data[7],
             issync);

      /* Check if we have a valid CODI sequence */

      if (data[7] != abnt_codi_checksum(data))
        {
          issync = false;
          continue;
        }

      /* If we arrived here we have a valid CODI sequence (we are in sync) */

      printf("Time since EPOCH time: %ld \n", time(NULL));

      sprintf(au8_sf_message, "%s%ld%X%X%X%X%X%X%X%X", AT_SF, time(NULL),
             data[0], data[1], data[2], data[3],
             data[4], data[5], data[6], data[7]);

      puts(au8_sf_message);

      /* Parse the received data */

      if (abnt_codi_parse(data, proto))
        {
          print_abnt_codi(proto);
        }
      else
        {
          err_count++;
        }

      /* If this is the first time, assume current missing seconds */

      if ( act_dem_ant == 0)
        {
          act_dem_ant = proto->end_act_dem;
        }

      /* The difference between previous read seconds should be <= 1 */

      if ((act_dem_ant - proto->end_act_dem) > 1)
        {
          issync = false;
        }
      else
        {
          issync = true;
        }

      /* Save act_dem_ant with current seconds to check against next value */

      act_dem_ant = proto->end_act_dem;

      /* If this is the end of current demand, set act_dem_ant = 900 (15min) */

      if (proto->end_act_dem == 0)
        {
          act_dem_ant = 900;

          /* Save this packet into EEPROM to be transmitted */

        }

      printf("There was %d errors until now\n", err_count);

      usleep(1000);
    }
}

int codi_init(const char *dev)
{
  int ret;
  pthread_t id_codi;

  /* Allocate memory to protocol struct */

  proto = malloc(sizeof(struct abnt_codi_proto_s));
  if (!proto)
    {
      printf("Failed to allocate memory to abnt_codi_proto_s!\n");
      return -ENOMEM;
    }

  /* Open the serial port used to read ABNT CODI (Baudrate: 110bps) */

  fd_codi = open(dev, O_RDONLY);
  if (fd_codi < 0)
    {
      printf(/*LOG_ERR,*/ "Error openning %s!\n", dev);
      ret = errno;
      goto init_file_err;
    }

  ret = pthread_create(&id_codi, NULL, (pthread_startroutine_t)codi_poll_data, NULL);
  if (ret != 0)
    {
      printf(/*LOG_ERR,*/ "codi_init: Failed to create codi poll thread, error=%d\n", ret);
      ret = -1;
      goto init_thread_err;
    }

  return OK;

init_thread_err:
  close(fd_codi);
init_file_err:
  free(proto);

  return ret;
}



/****************************************************************************
 * abntcodi_main
 ****************************************************************************/

#ifdef BUILD_MODULE
int main(int argc, FAR char *argv[])
#else
int abntcodi_main(int argc, char *argv[])
#endif
{
  int ret;

  printf("Starting the ABNT CODI!\n");

  ret = codi_init("/dev/ttyS0");
  if (ret < 0)
    {
      printf("Fail to open codi ttyS0\n");
      return -ENODEV;
    }

#if 0
  ret = sigfox_init("/dev/ttyS1");
  if (ret < 0)
    {
      printf("Fail to open SigFox ttyS1\n");
      return -ENODEV;
    }
#endif

  /* Stay here to keep the threads running */

  for (;;)
    {
      /* Nothing to do, then sleep to avoid eating all cpu time */

      usleep(10000);
    }

  return 0;
}
