#include <stdio.h>
#include <stdlib.h>

#include "fsutils/logbuf.h"

int testlogbuf_main(int argc, char *argv[])
{
  int i;
  int ret;
  int pktcnt;
  int num_pkt_write;
  int num_pkt_read;
  logentry save_pkt;
  logentry load_pkt;

  /* If user did't specify # pkts to write and read, use default */

  if (argc != 3)
    {
      num_pkt_write = 20;
      num_pkt_read  = 10;
    }
  else
    {
      num_pkt_write = atoi(argv[1]);
      num_pkt_read  = atoi(argv[2]);
    }

  /* Initialize the log system */

  ret = log_init();
  if (ret < 0)
    {
      printf("Error: Cannot initialize the Log system!\n");
      return -1;
    }

  printf("Going to save packets\n");
  printf("----------------------------------------------------------------------\n");
  
  for (i = 0; i < num_pkt_write; i++)
     {
        /* Create some data, it will be a sequence of 0-9 in ASCII */

        save_pkt.data[0]  = ((12*i + 0)  % 10) + '0';
        save_pkt.data[1]  = ((12*i + 1)  % 10) + '0';
        save_pkt.data[2]  = ((12*i + 2)  % 10) + '0';
        save_pkt.data[3]  = ((12*i + 3)  % 10) + '0';
        save_pkt.data[4]  = ((12*i + 4)  % 10) + '0';
        save_pkt.data[5]  = ((12*i + 5)  % 10) + '0';
        save_pkt.data[6]  = ((12*i + 6)  % 10) + '0';
        save_pkt.data[7]  = ((12*i + 7)  % 10) + '0';
        save_pkt.data[8]  = ((12*i + 8)  % 10) + '0';
        save_pkt.data[9]  = ((12*i + 9)  % 10) + '0';
        save_pkt.data[10] = ((12*i + 10) % 10) + '0';
        save_pkt.data[11] = ((12*i + 11) % 10) + '0';

        ret = insert_entry(&save_pkt);
        if (ret < 0)
          {
            printf("Failed to save packet #%d\n", i);
          }
         else
          {
            printf("Saving packet #%d\n", i);
            printf("----------------------------------------------------------------------\n");
          }
     }

  printf("Going to read packets\n");
  printf("----------------------------------------------------------------------\n");
  
  for (i = 0; i < num_pkt_read; i++)
     {
        ret = get_entry(&load_pkt);
        if (ret < 0)
          {
            printf("Failed to load packet #%d\n", i);
          }
         else
          {
            printf("Loading packet #%d\n", i);

            printf("PKT #%d Data[0]  = %c\n", load_pkt.pktcounter, load_pkt.data[0]);
            printf("PKT #%d Data[1]  = %c\n", load_pkt.pktcounter, load_pkt.data[1]);
            printf("PKT #%d Data[2]  = %c\n", load_pkt.pktcounter, load_pkt.data[2]);
            printf("PKT #%d Data[3]  = %c\n", load_pkt.pktcounter, load_pkt.data[3]);
            printf("PKT #%d Data[4]  = %c\n", load_pkt.pktcounter, load_pkt.data[4]);
            printf("PKT #%d Data[5]  = %c\n", load_pkt.pktcounter, load_pkt.data[5]);
            printf("PKT #%d Data[6]  = %c\n", load_pkt.pktcounter, load_pkt.data[6]);
            printf("PKT #%d Data[7]  = %c\n", load_pkt.pktcounter, load_pkt.data[7]);
            printf("PKT #%d Data[8]  = %c\n", load_pkt.pktcounter, load_pkt.data[8]);
            printf("PKT #%d Data[9]  = %c\n", load_pkt.pktcounter, load_pkt.data[9]);
            printf("PKT #%d Data[10] = %c\n", load_pkt.pktcounter, load_pkt.data[10]);
            printf("PKT #%d Data[11] = %c\n", load_pkt.pktcounter, load_pkt.data[11]);

            printf("----------------------------------------------------------------------\n");

            remove_entry(&load_pkt);
          }
     }

  return 0;
}
